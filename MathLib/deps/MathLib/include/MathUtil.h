#pragma once

#define EPSILON 0.0000001

//GJK Collision Algorithum
//http://www.dyn4j.org/2010/04/gjk-gilbert-johnson-keerthi/



float Signf(const float& a_arg)
{
	return (a_arg >= 0) ? 1.f : -1.f;
}
