#ifndef _MATRIX4_H_
#define _MATRIX4_H_

#include "Vector4.h"

class Matrix4
{
public:
	Matrix4();													//Contructor
	Matrix4(float, float, float,
		float, float, float, float, float, float,
		float, float, float, float, float, float, float);

	Matrix4(const Matrix4& a_Matrix3);							//Copy-Contructor - Added const so Matrix::IDENTITY can be used as a temp / unnamed variable
	~Matrix4();													//Detructor

	static const Matrix4 IDENTITY;

	void EmptyMatrix();										//Set all the Value in the Matrix to 0

	Vector4 VectorScale(Vector4& a_Vec, Matrix4& a_Matrix3);		//Finding the Vector after Scaling
	Vector4 VectorTranslate(Vector4& a_Vec, Matrix4& a_Matrix3);	//Finding the Vector after Translation
	Vector4 VectorRotate(Vector4& a_Vec, float a_fAngle);			//Finding the Vector after rotation

	void RotateZ(float a_fRotation);							//Rotate the Maxtrix around the Z axis
	void Scale(float a_fScale);									//Scale the Matrix

	void SetRow(Vector4& a_Vec, int a_iRow);					//Set a Row with a given Vector
	void GetRow(Vector4& a_Vec, int a_iRow);					//Get a Vector from a given row

	int GetValueSingle(int a_iRow, int a_iCol);					//Get a single Value from the Matrix
	int GetValueRow(int a_iRow);								//Get the value of a row from the Matrix
	int GetValueColunm(int a_iCol);								//Get the value of a Column from the Matrix
	int GetValueMatrix();										//Get the value of the Matrix
	int GetValueDiagonal(bool a_bDirection = true);				//Get the Value of the Diagonal (true = Top Left to Bottom Right, false = Bottom Left to Top Right)

																//Operator Overloads
	Matrix4 operator + (const Matrix4& a_Matrix3);				//Adding Matricies
	Matrix4 operator - (const Matrix4& a_Matrix3);				//Subtracting Matricies
	Matrix4 operator * (const Matrix4& a_Matrix3);				//Multiplying Matricies

	Vector4 operator * (Vector4& a_Vec);						//Multiplying by Vector

	operator float* ();
	operator const float*();

	int iRow;
	int iCol;


	union
	{
		float aMatrix2D[4][4];

		struct
		{
			float m_11, m_12, m_13, m_14;
			float m_21, m_22, m_23, m_24;
			float m_31, m_32, m_33, m_34;
			float m_41, m_42, m_43, m_44;
		};

		struct
		{
			float aMatrix1D[16];
		};

		struct
		{
			Vector4 xAxis;
			Vector4 yAxis;
			Vector4 zAxis;
			Vector4 Translation;
		};

	};



};

#endif