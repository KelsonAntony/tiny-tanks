#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include "Vector2.h"

class Vector3
{
public:

	Vector3();
	Vector3(float, float, float);										//Contructor
	Vector3(Vector2& a_vec, float a_fz = 0.f);
	~Vector3();															//Destructor

	static const Vector3 AXIS_X;
	static const Vector3 AXIS_Y;
	static const Vector3 AXIS_Z;
	static const Vector3 ZERO;

	float Magnitude();													//Magnitude of a Vector
	float MagnitudeSqrd();												//Magnited of a Vector Squared

	float Distance(const Vector3& a_vec1, const Vector3& a_vec2);		//Distance Between two vectors
	float DistanceSqrd(const Vector3& a_vec1, const Vector3& a_vec2);	//Distance Between two vectors Squared

	float DotProduct(const Vector3& a_vec1, const Vector3& a_vec2);		//DotProduct of a Vector ( x1 * x2 + y1 * y2)
	Vector3 CrossProduct(const Vector3& a_vec1, const Vector3& a_vec2);	//Cross Product of a Vector (y1z2 - z1y2, z1x2 - z2x1, x1y2 - x2y1)

	void Normalise();													//Normalise a Vector (x/y divided by the Magnitude (length))

																		//Operator Overloads
	Vector3 operator + (const Vector3& a_vec);							//Adding Vector
	Vector3 operator + (const float & a_fval);							//Adding Vector with Value

	Vector3 operator - (const Vector3& a_vec);							//Subtracing Vector
	Vector3 operator - (const float& a_fval);							//Subtracting Vector with Value

	Vector3 operator * (const Vector3& a_vec);							//Timsing Vector
	Vector3 operator * (const float& a_fval);							//Timsing Vector with Value

	Vector3 operator / (const Vector3& a_vec);							//Dividing Vector
	Vector3 operator / (const float& a_fval);							//Dividing Vector with Value

	bool operator != (const Vector3& a_vec);							//Not Equal to
	bool operator == (const Vector3& a_vec);							//Equivilent to

	Vector3 operator *= (const Vector3& a_vec);							//Shorthand Vec1 = Vec1 * Vec2
	Vector3 operator *= (const float& a_fval);							//Shorthand Vec1 = Vec1 * Float

	Vector3 operator += (const Vector3& a_vec);							//Shorthand Vec1 = Vec1 + Vex2
	Vector3 operator += (const float& a_fval);							//Shorthand Vec1 = Vec1 + Float

	operator float* ();
	operator const float*();

	float fx;
	float fy;
	float fz;

};

#endif