#ifndef _VECTOR2_H_
#define _VECTOR2_H_

class Vector2
{
public:

	Vector2();															//Contructor
	Vector2(float, float);											
	~Vector2();															//Destructor

	static const Vector2 AXIS_Y;
	static const Vector2 AXIS_X;
	static const Vector2 ZERO;

	void GetVectorValue(float& a_rfx, float& a_rfy);

	float Magnitude();													//Magnitude of a Vector
	float MagnitudeSqrd();												//Magnited of a Vector Squared

	float Distance(const Vector2& a_vec1, const Vector2& a_vec2);		//Distance Between two vectors
	float DistanceSqrd(const Vector2& a_vec1, const Vector2& a_vec2);	//Distance Between two vectors Squared

	float DotProduct(const Vector2& a_vec1, const Vector2& a_vec2);		//DotProduct of a Vector ( x1 * x2 + y1 * y2)

	void Normalise();													//Normalise a Vector (x/y Magnitude (length))

	//Creating Curves / Lines based on vector positions
	Vector2 Lerp(Vector2 a_Vec1, Vector2 a_Vec2, float a_fTime);											//Liner Interpolation
	Vector2 Bezier(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_Vec3, float a_ftime);							//Bezier Curves
	Vector2 Hermite(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_VecTan1, Vector2 a_VecTan2, float a_ft);		//Hermite Splines
	Vector2 Cardinal(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_Vec3, float a_fa, float a_ft);				//Cardinal Splines
	Vector2 CatmullRom(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_Vec3, float a_ft);							//CatmullRom Spline (Cardinal Spline with a set to 0.5)

	//Operator Overloads
	Vector2 operator + (const Vector2& a_vec);							//Adding Vector
	Vector2 operator + (const float & a_fval);							//Adding Vector with Value

	Vector2 operator - (const Vector2& a_vec);							//Subtracing Vector
	Vector2 operator - (const float& a_fval);							//Subtracting Vector with Value

	Vector2 operator * (const Vector2& a_vec);							//Timsing Vector
	Vector2 operator * (const float& a_fval);							//Timsing Vector with Value

	Vector2 operator / (const Vector2& a_vec);							//Dividing Vector
	Vector2 operator / (const float& a_fval);							//Dividing Vector with Value

	bool operator != (const Vector2& a_vec);							//Not Equal to 
	bool operator == (const Vector2& a_vec);							//Equivilent to

	operator float*();
	operator const float*() const;

	float fx;
	float fy;
	
};

#endif // !_VECTOR2_H_