#ifndef _MATRIX3_H_
#define _MATRIX3_H_

#include "Vector3.h"

class Matrix3
{
public:
	Matrix3();													//Contructor
	Matrix3(float, float, float,
		float, float, float, float, float, float);
	
	Matrix3(const Matrix3& a_Matrix3);							//Copy-Contructor - Added const so Matrix::IDENTITY can be used as a temp / unnamed variable
	~Matrix3();													//Detructor

	static const Matrix3 IDENTITY;

	void EmptyMatrix();										//Set all the Value in the Matrix to 0
	
	Vector3 VectorScale(Vector3& a_Vec, Matrix3& a_Matrix3);		//Finding the Vector after Scaling
	Vector3 VectorTranslate(Vector3& a_Vec, Matrix3& a_Matrix3);	//Finding the Vector after Translation
	Vector3 VectorRotate(Vector3& a_Vec, float a_fAngle);			//Finding the Vector after rotation

	void RotateZ(float a_fRotation);							//Rotate the Maxtrix around the Z axis
	void Scale(float a_fScale);									//Scale the Matrix

	void SetRow(Vector3& a_Vec, int a_iRow);					//Set a Row with a given Vector
	void GetRow(Vector3& a_Vec, int a_iRow);					//Get a Vector from a given row

	int GetValueSingle(int a_iRow, int a_iCol);					//Get a single Value from the Matrix
	int GetValueRow(int a_iRow);								//Get the value of a row from the Matrix
	int GetValueColunm(int a_iCol);								//Get the value of a Column from the Matrix
	int GetValueMatrix();										//Get the value of the Matrix
	int GetValueDiagonal(bool a_bDirection = true);				//Get the Value of the Diagonal (true = Top Left to Bottom Right, false = Bottom Left to Top Right)

	//Operator Overloads
	Matrix3 operator + (const Matrix3& a_Matrix3);				//Adding Matricies
	Matrix3 operator - (const Matrix3& a_Matrix3);				//Subtracting Matricies
	Matrix3 operator * (const Matrix3& a_Matrix3);				//Multiplying Matricies

	Vector3 operator * (Vector3& a_Vec);						//Multiplying by Vector

	int iRow;
	int iCol;


	union 
	{
		float aMatrix2D[3][3];
		
		struct 
		{
			float m_11,	m_12, m_13;
			float m_21, m_22, m_23;
			float m_31, m_32, m_33;
		};

		struct 
		{
			float aMatrix1D[9];
		};
		
		struct 
		{
			Vector3 xAxis;
			Vector3 yAxis;
			union
			{
				Vector3 zAxis;
				Vector3 Translation;
			};
		};

	};



};

#endif // !_MATRIX3_H_