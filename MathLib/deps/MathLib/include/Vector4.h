#ifndef _VECTOR4_H_
#define _VECTOR4_H_

#include "Vector3.h"

class Vector4
{
public:

	Vector4();															//Contructor
	Vector4(float, float, float, float);
	Vector4(Vector3& a_vec, float a_fw = 0.f);

	~Vector4();															//Destructor

	static const Vector4 AXIS_X;
	static const Vector4 AXIS_Y;
	static const Vector4 AXIS_Z;
	static const Vector4 ZERO;

	//Operator Overloads
	Vector4 operator + (const Vector4& a_vec);							//Adding Vector
	Vector4 operator + (const float & a_fval);							//Adding Vector with Value

	Vector4 operator - (const Vector4& a_vec);							//Subtracing Vector
	Vector4 operator - (const float& a_fval);							//Subtracting Vector with Value

	Vector4 operator * (const Vector4& a_vec);							//Timsing Vector
	Vector4 operator * (const float& a_fval);							//Timsing Vector with Value

	Vector4 operator / (const Vector4& a_vec);							//Dividing Vector
	Vector4 operator / (const float& a_fval);							//Dividing Vector with Value

	bool operator != (const Vector4& a_vec);							//Not Equal to
	bool operator == (const Vector4& a_vec);							//Equivilent to

	operator float* ();
	operator const float*();

	float fx;
	float fy;
	float fz;
	float fw;

};



#endif
