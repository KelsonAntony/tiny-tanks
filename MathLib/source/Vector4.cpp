#include "Vector4.h"

Vector4::Vector4() : fx(0.f), fy(0.f), fz(0.f), fw(0.f)
{

}

Vector4::Vector4(float a_fx, float a_fy, float a_fz, float a_fw)
{
	fx = a_fx;
	fy = a_fy;
	fz = a_fz;
	fw = a_fw;
}

Vector4::Vector4(Vector3& a_vec, float a_fw) : fx(a_vec.fx), fy(a_vec.fy), fz(a_vec.fz), fw(a_fw)
{

}

Vector4::~Vector4()
{}

const Vector4 AXIS_X = Vector4(1.f, 0.f, 0.f, 0.f);
const Vector4 AXIS_Y = Vector4(0.f, 1.f, 0.f, 0.f);
const Vector4 AXIS_Z = Vector4(0.f, 0.f, 1.f, 0.f);
const Vector4 ZERO   = Vector4(0.f, 0.f, 0.f, 0.f);

Vector4 Vector4::operator + (const Vector4& a_vec)
{
	return Vector4(fx + a_vec.fx, fy + a_vec.fy, fz + a_vec.fz, fw + a_vec.fw);
}

Vector4 Vector4::operator + (const float & a_fval)
{
	return Vector4(fx + a_fval, fy + a_fval, fz + a_fval, fw + a_fval);
}

Vector4 Vector4::operator - (const Vector4& a_vec)
{
	return Vector4(fx - a_vec.fx, fy - a_vec.fy, fz - a_vec.fz, fw - a_vec.fw);
}

Vector4 Vector4::operator - (const float& a_fval)
{
	return Vector4(fx - a_fval, fy - a_fval, fz - a_fval, fw - a_fval);
}

Vector4 Vector4::operator * (const Vector4& a_vec)
{
	return Vector4(fx * a_vec.fx, fy * a_vec.fy, fz * a_vec.fz, fw * a_vec.fw);
}

Vector4 Vector4::operator * (const float& a_fval)
{
	return Vector4(fx * a_fval, fy * a_fval, fz * a_fval, fw * a_fval);
}

Vector4 Vector4::operator / (const Vector4& a_vec)
{
	return Vector4(fx / a_vec.fx, fy / a_vec.fy, fz / a_vec.fz, fw / a_vec.fw);
}

Vector4 Vector4::operator / (const float& a_fval)
{
	return Vector4(fx / a_fval, fy / a_fval, fz / a_fval, fw / a_fval);
}

bool Vector4::operator != (const Vector4& a_vec)
{
	return (fx != a_vec.fx && fy != a_vec.fy && fz != a_vec.fz && fw != a_vec.fw);
}

bool Vector4::operator == (const Vector4& a_vec)
{
	return (fx == a_vec.fx && fy == a_vec.fy && fz == a_vec.fz && fw == a_vec.fw);
}

Vector4::operator float *()
{
	return static_cast<float*>(&fx);
}

Vector4::operator const float *()
{
	return const_cast<float*>(&fx);
}