#include <math.h>

#include "Matrix4.h"

const Matrix4 Matrix4::IDENTITY = Matrix4(  1.f, 0.f, 0.f, 0.f,
											0.f, 1.f, 0.f, 0.f,
											0.f, 0.f, 1.f, 0.f,
											0.f, 0.f, 0.f, 1.f);

Matrix4::Matrix4()
{
	iRow = 4;
	iCol = 4;
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			aMatrix2D[i][j] = 0;
		}
	}
}

Matrix4::Matrix4(float a_f1, float a_f2, float a_f3, float a_f4, float a_f5, float a_f6, float a_f7, float a_f8, float a_f9,
	float a_f10, float a_f11, float a_f12, float a_f13, float a_f14, float a_f15, float a_f16)
{
	iRow = 4;
	iCol = 4;
	m_11 = a_f1;  m_12 = a_f2;  m_13 = a_f3;  m_14 = a_f4;
	m_21 = a_f5;  m_22 = a_f6;  m_23 = a_f7;  m_24 = a_f8;
	m_31 = a_f9;  m_32 = a_f10; m_33 = a_f11; m_34 = a_f12;
	m_41 = a_f13; m_42 = a_f14;	m_43 = a_f15; m_44 = a_f16;
}

Matrix4::Matrix4(const Matrix4& a_Matrix4)
{
	iRow = a_Matrix4.iRow;
	iCol = a_Matrix4.iCol;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			aMatrix2D[i][j] = a_Matrix4.aMatrix2D[i][j];
		}
	}
}

Matrix4::~Matrix4()
{
}

void Matrix4::EmptyMatrix()
{
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			aMatrix2D[i][j] = 0;
		}
	}
}

Vector4 Matrix4::VectorScale(Vector4& a_Vec, Matrix4& a_Matrix4)
{
	return Vector4(a_Matrix4*a_Vec);
}

Vector4 Matrix4::VectorTranslate(Vector4& a_Vec, Matrix4& a_Matrix4)
{
	return Vector4(a_Matrix4*a_Vec);
}

Vector4 Matrix4::VectorRotate(Vector4& a_Vec, float a_fAngle)
{
	Matrix4 MatrixAngle;

	MatrixAngle.aMatrix2D[0][0] = cosf(a_fAngle);
	MatrixAngle.aMatrix2D[0][1] = sinf(a_fAngle);

	MatrixAngle.aMatrix2D[1][0] = (sinf(a_fAngle)) * -1;
	MatrixAngle.aMatrix2D[1][1] = cosf(a_fAngle);

	return Vector4(MatrixAngle*a_Vec);
}

void Matrix4::RotateZ(float a_fRotation)
{
	//New X
	aMatrix2D[0][0] = cosf(a_fRotation);
	aMatrix2D[0][1] = sinf(a_fRotation);

	//New Y
	aMatrix2D[1][0] = (sinf(a_fRotation)) * -1;
	aMatrix2D[1][1] = cosf(a_fRotation);
}

void Matrix4::Scale(float a_fScale)
{
	for (int i = 0; i < iRow; i++)
	{
		aMatrix2D[i][i] = aMatrix2D[i][i] * a_fScale;
	}
}

void Matrix4::SetRow(Vector4& a_Vec, int a_iRow)
{
	aMatrix2D[a_iRow][0] = a_Vec.fx;
	aMatrix2D[a_iRow][1] = a_Vec.fy;
	aMatrix2D[a_iRow][2] = a_Vec.fz;
	aMatrix2D[a_iRow][3] = a_Vec.fw;
}

void Matrix4::GetRow(Vector4& a_Vec, int a_iRow)
{
	a_Vec.fx = aMatrix2D[a_iRow][0];
	a_Vec.fy = aMatrix2D[a_iRow][1];
	a_Vec.fz = aMatrix2D[a_iRow][2];
	a_Vec.fw = aMatrix2D[a_iRow][3];
}

int Matrix4::GetValueSingle(int a_iRow, int a_iCol)
{
	return aMatrix2D[a_iRow - 1][a_iCol - 1];
}

int Matrix4::GetValueRow(int a_iRow)
{
	int iRowValue = 0;

	for (int i = 0; i < iCol; i++)
	{
		iRowValue += aMatrix2D[a_iRow][i];
	}
	return iRowValue;
}

int Matrix4::GetValueColunm(int a_iCol)
{
	int iColValue = 0;

	for (int i = 0; i < iRow; i++)
	{
		iColValue += aMatrix2D[i][a_iCol];
	}
	return iColValue;
}

int Matrix4::GetValueMatrix()
{
	int iMatrixValue = 0;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			iMatrixValue += aMatrix2D[i][j];
		}
	}

	return iMatrixValue;
}

int Matrix4::GetValueDiagonal(bool a_bDirection)
{
	int iDiagValue = 0;

	if (a_bDirection)
	{
		for (int i = 0; i < iRow; i++)
		{
			iDiagValue += aMatrix2D[i][i];
		}
	}
	else
	{
		int j = (iCol - 1);
		for(int i = 0; i < iRow; i++)
		{
			iDiagValue += aMatrix2D[i][j];
			j--;
		}
	}

	return iDiagValue;
}

Matrix4 Matrix4::operator + (const Matrix4& a_Matrix4)
{
	Matrix4 TempMatrix;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			TempMatrix.aMatrix2D[i][j] = aMatrix2D[i][j] + a_Matrix4.aMatrix2D[i][j];
		}
	}

	return Matrix4(TempMatrix);
}

Matrix4 Matrix4::operator - (const Matrix4& a_Matrix4)
{
	Matrix4 TempMatrix;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			TempMatrix.aMatrix2D[i][j] = aMatrix2D[i][j] - a_Matrix4.aMatrix2D[i][j];
		}
	}

	return Matrix4(TempMatrix);
}

Matrix4 Matrix4::operator * (const Matrix4& a_Matrix4)
{
	Matrix4 TempMatrix;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			TempMatrix.aMatrix2D[i][j] = (aMatrix2D[i][0] * a_Matrix4.aMatrix2D[0][j]) + (aMatrix2D[i][1] * a_Matrix4.aMatrix2D[1][j]) + (aMatrix2D[i][2] * a_Matrix4.aMatrix2D[2][j]);
		}
	}	//  [1][0] * [0][1] + [1][1] * [1][1] + [2][1] * [1][2] repeat

	return Matrix4(TempMatrix);
}

Vector4 Matrix4::operator * (Vector4& a_Vec)
{
	float fx, fy, fz, fw;

	fx = aMatrix2D[0][0] * a_Vec.fx + aMatrix2D[1][0] * a_Vec.fy + aMatrix2D[2][0] * a_Vec.fz + aMatrix2D[3][0] * a_Vec.fw;
	fy = aMatrix2D[0][1] * a_Vec.fx + aMatrix2D[1][1] * a_Vec.fy + aMatrix2D[2][1] * a_Vec.fz + aMatrix2D[3][1] * a_Vec.fw;
	fz = aMatrix2D[0][2] * a_Vec.fx + aMatrix2D[1][2] * a_Vec.fy + aMatrix2D[2][2] * a_Vec.fz + aMatrix2D[3][2] * a_Vec.fw;
	fw = aMatrix2D[0][3] * a_Vec.fx + aMatrix2D[1][3] * a_Vec.fy + aMatrix2D[2][3] * a_Vec.fz + aMatrix2D[3][3] * a_Vec.fw;

	return Vector4(fx, fy, fz, fw);
}

Matrix4::operator float *()
{
	return static_cast<float*>(aMatrix1D);
}

Matrix4::operator const float *()
{
	return const_cast<float*>(aMatrix1D);
}

