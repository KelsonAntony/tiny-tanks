#include <math.h>

#include "Matrix3.h"

const Matrix3 Matrix3::IDENTITY = Matrix3(  1.f ,0.f ,0.f ,
											0.f, 1.f, 0.f, 
											0.f, 0.f, 1.f);

Matrix3::Matrix3()
{
	iRow = 3;
	iCol = 3;
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			aMatrix2D[i][j] = 0;
		}
	}
}

Matrix3::Matrix3(float a_f1, float a_f2, float a_f3, float a_f4, float a_f5, float a_f6, float a_f7, float a_f8, float a_f9)
{
	iRow = 3;
	iCol = 3;
	m_11 = a_f1; m_12 = a_f2; m_13 = a_f3;
	m_21 = a_f4; m_22 = a_f5; m_23 = a_f6;
	m_31 = a_f7; m_32 = a_f8; m_33 = a_f9;
}

Matrix3::Matrix3(const Matrix3& a_Matrix3)
{
	iRow = 3;
	iCol = 3;
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			aMatrix2D[i][j] = a_Matrix3.aMatrix2D[i][j];
		}
	}
}

Matrix3::~Matrix3()
{
}

void Matrix3::EmptyMatrix()
{
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			aMatrix2D[i][j] = 0;
		}
	}
}

Vector3 Matrix3::VectorScale(Vector3& a_Vec, Matrix3& a_Matrix3)
{
	return Vector3(a_Matrix3*a_Vec);
}

Vector3 Matrix3::VectorTranslate(Vector3& a_Vec, Matrix3& a_Matrix3)
{
	return Vector3(a_Matrix3*a_Vec);
}

Vector3 Matrix3::VectorRotate(Vector3& a_Vec, float a_fAngle)
{
	Matrix3 MatrixAngle;
	
	MatrixAngle.aMatrix2D[0][0] = cosf(a_fAngle);
	MatrixAngle.aMatrix2D[0][1] = sinf(a_fAngle);

	MatrixAngle.aMatrix2D[1][0] = (sinf(a_fAngle)) * -1;
	MatrixAngle.aMatrix2D[1][1] = cosf(a_fAngle);

	return Vector3(MatrixAngle*a_Vec);
}

void Matrix3::RotateZ(float a_fRotation)
{
	//New X
	aMatrix2D[0][0] = cosf(a_fRotation);
	aMatrix2D[0][1] = sinf(a_fRotation);

	//New Y
	aMatrix2D[1][0] = -sinf(a_fRotation);
	aMatrix2D[1][1] = cosf(a_fRotation);
}

void Matrix3::Scale(float a_fScale)
{
	for (int i = 0; i < iRow; i++)
	{
		aMatrix2D[i][i] = aMatrix2D[i][i] * a_fScale;
	}
}

void Matrix3::SetRow(Vector3& a_Vec, int a_iRow)
{
	aMatrix2D[a_iRow][0] = a_Vec.fx;
	aMatrix2D[a_iRow][1] = a_Vec.fy;
	aMatrix2D[a_iRow][2] = a_Vec.fz;
}

void Matrix3::GetRow(Vector3& a_Vec, int a_iRow)
{
	a_Vec.fx = aMatrix2D[a_iRow][0];
	a_Vec.fy = aMatrix2D[a_iRow][1];
	a_Vec.fz = aMatrix2D[a_iRow][2];
}

int Matrix3::GetValueSingle(int a_iRow, int a_iCol)
{
	return aMatrix2D[a_iRow-1][a_iCol-1];
}

int Matrix3::GetValueRow(int a_iRow)
{
	int iRowValue = 0;

	for (int i = 0; i < 3; i++)
	{
		iRowValue += aMatrix2D[a_iRow][i];
	}
	return iRowValue;
}

int Matrix3::GetValueColunm(int a_iCol)
{
	int iColValue = 0;

	for (int i = 0; i < 3; i++)
	{
		iColValue += aMatrix2D[i][a_iCol];
	}
	return iColValue;
}

int Matrix3::GetValueMatrix()
{
	int iMatrixValue = 0;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			iMatrixValue += aMatrix2D[i][j];
		}
	}

	return iMatrixValue;
}

int Matrix3::GetValueDiagonal(bool a_bDirection)
{
	int iDiagValue = 0;

	if (a_bDirection)
	{
		for (int i = 0; i < iRow; i++)
		{
			iDiagValue += aMatrix2D[i][i];
		}
	}
	else
	{
		int j = (iCol - 1);
		for (int i = 0; i < iRow; i++)
		{
			iDiagValue += aMatrix2D[i][j];
			j--;
		}
	}

	return iDiagValue;
}

Matrix3 Matrix3::operator + (const Matrix3& a_Matrix3)
{
	Matrix3 TempMatrix;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			TempMatrix.aMatrix2D[i][j] = aMatrix2D[i][j] + a_Matrix3.aMatrix2D[i][j];
		}
	}
	
	return Matrix3(TempMatrix);
}

Matrix3 Matrix3::operator - (const Matrix3& a_Matrix3)
{
	Matrix3 TempMatrix;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			TempMatrix.aMatrix2D[i][j] = aMatrix2D[i][j] - a_Matrix3.aMatrix2D[i][j];
		}
	}

	return Matrix3(TempMatrix);
}

Matrix3 Matrix3::operator * (const Matrix3& a_Matrix3)
{
	Matrix3 TempMatrix;

	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < iCol; j++)
		{
			TempMatrix.aMatrix2D[i][j] = (aMatrix2D[i][0] * a_Matrix3.aMatrix2D[0][j]) + (aMatrix2D[i][1] * a_Matrix3.aMatrix2D[1][j]) + (aMatrix2D[i][2] * a_Matrix3.aMatrix2D[2][j]);
		}
	}	//  [1][0] * [0][1] + [1][1] * [1][1] + [2][1] * [1][2] repeat

	return Matrix3(TempMatrix);
}

Vector3 Matrix3::operator * (Vector3& a_Vec)
{
	float fx, fy, fz;
	fx = a_Vec.fx * aMatrix2D[0][0] + a_Vec.fy * aMatrix2D[1][0] + a_Vec.fz * aMatrix2D[2][0];
	fy = a_Vec.fx * aMatrix2D[0][1] + a_Vec.fy * aMatrix2D[1][1] + a_Vec.fz * aMatrix2D[2][1];
	fz = a_Vec.fx * aMatrix2D[0][2] + a_Vec.fy * aMatrix2D[1][2] + a_Vec.fz * aMatrix2D[2][2];

	return Vector3(fx, fy, fz);
}
