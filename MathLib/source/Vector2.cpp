#include "Vector2.h"
#include <math.h>

Vector2::Vector2() : fx(0.f), fy(0.f)
{}

Vector2::Vector2(float a_fx, float a_fy)
{
	fx = a_fx;
	fy = a_fy;
}

Vector2::~Vector2()
{}

const Vector2 Vector2::AXIS_X = Vector2(1.f, 0.f);
const Vector2 Vector2::AXIS_Y = Vector2(0.f, 1.f);
const Vector2 Vector2::ZERO   = Vector2(0.f, 0.f);

void Vector2::GetVectorValue(float& a_rfx, float& a_rfy)
{
	a_rfx = fx;
	a_rfy = fy;
}

float Vector2::Magnitude()
{
	return sqrtf(fx * fx + fy * fy);
}

float Vector2::MagnitudeSqrd()
{
	return fx * fx + fy * fy;
}

float Vector2::Distance(const Vector2& a_vec1, const Vector2& a_vec2)
{
	float fxDistance = a_vec1.fx - a_vec2.fx;
	float fyDistance = a_vec1.fy - a_vec2.fy;
	return sqrtf(fxDistance * fxDistance + fyDistance * fyDistance);
}

float Vector2::DistanceSqrd(const Vector2& a_vec1, const Vector2& a_vec2)
{
	float fxDistance = a_vec1.fx - a_vec2.fx;
	float fyDistance = a_vec1.fy - a_vec2.fy;
	return fxDistance * fxDistance + fyDistance * fyDistance;
}

float Vector2::DotProduct(const Vector2& a_vec1, const Vector2& a_vec2)
{
	return a_vec1.fx * a_vec2.fx + a_vec1.fy * a_vec2.fy;
}

void Vector2::Normalise()
{
	float fMagnitude = Magnitude();
	if (fMagnitude != 0)
	{
		fx = fx / fMagnitude;
		fy = fy / fMagnitude;
	}
}

Vector2 Vector2::Lerp(Vector2 a_Vec1, Vector2 a_Vec2, float a_fTime)
{
	return (a_Vec2 * a_fTime) + (a_Vec1 * (1 - a_fTime));
}

Vector2 Vector2::Bezier(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_Vec3, float a_ftime)
{
	Vector2 Point1 = Lerp(a_Vec1, a_Vec2, a_ftime);		//Lerp for the first point (curve)
	Vector2 Point2 = Lerp(a_Vec2, a_Vec3, a_ftime);		//Lerp for the second point (destination)

	return Lerp(Point1, Point2, a_ftime);
}

Vector2 Vector2::Hermite(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_VecTan1, Vector2 a_VecTan2, float a_ft)
{
	float fTsq = a_ft * a_ft;
	float fTcub = fTsq * a_ft;

	float fH00 = 2 * fTcub - 3 * fTsq + 1;
	float fH01 = -2 * fTcub + 3 * fTsq;
	float fH10 = fTcub - 2 * fTsq + a_ft;
	float fH11 = fTcub - fTsq;

	Vector2 Point = a_Vec1 * fH00 + a_VecTan1 * fH10 + a_Vec2 * fH01 + a_VecTan2 * fH11;
	return Point;
}

Vector2 Vector2::Cardinal(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_Vec3, float a_fa, float a_ft)
{
	Vector2 VecTan1 = (a_Vec2 - a_Vec1) * a_fa;
	Vector2 VecTan2 = (a_Vec3 - a_Vec2) * a_fa;

	float fTsq = a_ft * a_ft;
	float fTcub = fTsq * a_ft;

	float fH00 = 2 * fTcub - 3 * fTsq + 1;
	float fH01 = -2 * fTcub + 3 * fTsq;
	float fH10 = fTcub - 2 * fTsq + a_ft;
	float fH11 = fTcub - fTsq;

	Vector2 Point = a_Vec1 * fH00 + VecTan1 * fH10 + a_Vec2 * fH01 + VecTan2 * fH11;
	return Point;
}

Vector2 Vector2::CatmullRom(Vector2 a_Vec1, Vector2 a_Vec2, Vector2 a_Vec3, float a_ft)
{
	Vector2 CatmullSpline = Cardinal(a_Vec1,a_Vec2 ,a_Vec3 ,0.5 ,a_ft );
	return CatmullSpline;
}

Vector2 Vector2::operator + (const Vector2& a_vec)
{
	return Vector2(fx + a_vec.fx, fy + a_vec.fy);
}

Vector2 Vector2::operator + (const float& a_fval)
{
	return Vector2(fx + a_fval, fy + a_fval);
}

Vector2 Vector2::operator - (const Vector2& a_vec)
{
	return Vector2(fx - a_vec.fx, fy - a_vec.fy);
}

Vector2 Vector2::operator - (const float& a_fval)
{
	return Vector2(fx - a_fval, fy - a_fval);
}

Vector2 Vector2::operator * (const Vector2& a_vec)
{
	return Vector2(fx * a_vec.fx, fy * a_vec.fy);
}

Vector2 Vector2::operator * (const float& a_fval)
{
	return Vector2(fx * a_fval, fy * a_fval);
}

Vector2 Vector2::operator / (const Vector2& a_vec)
{
	return Vector2(fx / a_vec.fx, fy / a_vec.fy);
}

Vector2 Vector2::operator / (const float& a_fval)
{
	return Vector2(fx / a_fval, fy / a_fval);
}

bool Vector2::operator != (const Vector2& a_vec)
{
	return (fx != a_vec.fx && fy != a_vec.fy);
}

bool Vector2::operator == (const Vector2& a_vec)
{
	return (fx == a_vec.fx && fy == a_vec.fy);
}

Vector2::operator float *()
{
	return static_cast<float*>(&fx);
}

Vector2::operator const float*() const
{
	return const_cast<float*>(&fx);
}

