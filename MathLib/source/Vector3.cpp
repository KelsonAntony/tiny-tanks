#include "Vector3.h"
#include <math.h>

const Vector3 Vector3::AXIS_X = Vector3(1.f, 0.f, 0.f);
const Vector3 Vector3::AXIS_Y = Vector3(0.f, 1.f, 0.f);
const Vector3 Vector3::AXIS_Z = Vector3(0.f, 0.f, 1.f);
const Vector3 Vector3::ZERO   = Vector3(0.f, 0.f, 0.f);

Vector3::Vector3() : fx(0.f), fy(0.f), fz(0.f)
{

}

Vector3::Vector3(float a_fx, float a_fy, float a_fz)
{
	fx = a_fx;
	fy = a_fy;
	fz = a_fz;
}

Vector3::Vector3(Vector2& a_vec, float a_fz) : fx(a_vec.fx), fy(a_vec.fy), fz(a_fz)
{
	
}

Vector3::~Vector3()
{
}

float Vector3::Magnitude()
{
	return sqrtf(fx * fx + fy * fy + fz * fz);
}

float Vector3::MagnitudeSqrd()
{
	return fx * fx + fy * fy + fz * fz;
}

float Vector3::Distance(const Vector3& a_vec1, const Vector3& a_vec2)
{
	float fxDistance = a_vec1.fx - a_vec2.fx;
	float fyDistance = a_vec1.fy - a_vec2.fy;
	float fzDistance = a_vec1.fz - a_vec2.fz;
	return sqrtf(fxDistance * fxDistance + fyDistance * fyDistance + fzDistance * fzDistance);
}

float Vector3::DistanceSqrd(const Vector3& a_vec1, const Vector3& a_vec2)
{
	float fxDistance = a_vec1.fx - a_vec2.fx;
	float fyDistance = a_vec1.fy - a_vec2.fy;
	float fzDistance = a_vec1.fz - a_vec2.fz;
	return fxDistance * fxDistance + fyDistance * fyDistance + fzDistance * fzDistance;
}

float Vector3::DotProduct(const Vector3& a_vec1, const Vector3& a_vec2)
{
	return a_vec1.fx * a_vec2.fx + a_vec1.fy * a_vec2.fy + a_vec1.fz * a_vec2.fz;
}

Vector3 Vector3::CrossProduct(const Vector3& a_vec1, const Vector3& a_vec2)
{
	return Vector3(a_vec1.fy*a_vec2.fz - a_vec1.fz*a_vec2.fy, a_vec1.fz*a_vec2.fx - a_vec1.fx*a_vec2.fz, a_vec1.fx*a_vec2.fy - a_vec1.fy*a_vec2.fx);
}	//(y1z2 - z1y2, z1x2 - z2x1, x1y2 - x2y1)

void Vector3::Normalise()
{
	float fMagnitude = Magnitude();
	if (fMagnitude != 0)
	{
		fx = fx / fMagnitude;
		fy = fy / fMagnitude;
		fz = fz / fMagnitude;
	}
}

Vector3 Vector3::operator + (const Vector3& a_vec)
{
	return Vector3(fx + a_vec.fx, fy + a_vec.fy, fz + a_vec.fz);
}

Vector3 Vector3::operator + (const float& a_fval)
{
	return Vector3(fx + a_fval, fy + a_fval, fz + a_fval);
}

Vector3 Vector3::operator - (const Vector3& a_vec)
{
	return Vector3(fx - a_vec.fx, fy - a_vec.fy, fz - a_vec.fz);
}

Vector3 Vector3::operator - (const float& a_fval)
{
	return Vector3(fx - a_fval, fy - a_fval, fz - a_fval);
}

Vector3 Vector3::operator * (const Vector3& a_vec)
{
	return Vector3(fx * a_vec.fx, fy * a_vec.fy, fz * a_vec.fz);
}

Vector3 Vector3::operator * (const float& a_fval)
{
	return Vector3(fx * a_fval, fy * a_fval, fz * a_fval);
}

Vector3 Vector3::operator / (const Vector3& a_vec)
{
	return Vector3(fx / a_vec.fx, fy / a_vec.fy, fz / a_vec.fz);
}

Vector3 Vector3::operator / (const float& a_fval)
{
	return Vector3(fx / a_fval, fy / a_fval, fz / a_fval);
}

bool Vector3::operator != (const Vector3& a_vec)
{
	return (fx != a_vec.fx && fy != a_vec.fy && fz != a_vec.fz);
}

bool Vector3::operator == (const Vector3& a_vec)
{
	return (fx == a_vec.fx && fy == a_vec.fy && fz == a_vec.fz);
}

Vector3 Vector3::operator *= (const Vector3& a_vec)
{
	fx *= a_vec.fx;
	fy *= a_vec.fy;
	fz *= a_vec.fz;

	return *this;
}

Vector3 Vector3::operator *= (const float& a_fval)
{
	fx *= a_fval;
	fy *= a_fval;
	fz *= a_fval;

	return *this;
}

Vector3 Vector3::operator += (const Vector3& a_vec)
{
	fx += a_vec.fx;
	fy += a_vec.fy;
	fz += a_vec.fz;

	return *this;
}

Vector3 Vector3::operator += (const float& a_fval)
{
	fx += a_fval;
	fy += a_fval;
	fz += a_fval;

	return *this;
}

Vector3::operator float *()
{
	return static_cast<float*>(&fx);
}

Vector3::operator const float *()
{
	return const_cast<float*>(&fx);
}