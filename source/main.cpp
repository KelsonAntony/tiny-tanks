#include "UGFW.h"
#include "Enumerations.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix3.h"
#include "Matrix4.h"
#include "MathUtil.h"
#include "Node.h"
#include "Sprite.h"

#include <cmath>
#include <iostream>
#include <set>

int main(int argv, char* argc[])
{
	if (UG::Create( 1024, 768, false, "Tiny Tanks", 100, 100 ))
	{

		int iScreenWidth = 0, iScreenHeight = 0;
		UG::GetScreenSize(iScreenWidth, iScreenHeight);

		UG::SetBackgroundColor(UG::SColour(0x2A, 0x57, 0x66, 0xFF));
		UG::AddFont("./fonts/invaders.fnt");
		
		//Create a sprite for our tank's base
		Sprite* pTank = new Sprite("./images/tanks.png", 66, 72, Vector2(0.5f, 0.5f), Vector4(0.058f, 0.536f, 0.442f, 0.964f));
		pTank->SetPosition(Vector2(iScreenWidth * 0.5f, iScreenHeight * 0.5f));
		pTank->MarkForDraw();
		pTank->SetLayer(0);

		//Create a sprite for our tank's turret
		Sprite* pTurret = new Sprite("./images/tanks.png", 38, 64, Vector2(0.5f, 0.29f), Vector4(0.622f, 0.607f, 0.843f, 0.988f));
		pTurret->SetParent(pTank);
		pTurret->MarkForDraw();
		pTurret->SetLayer(1);

		//Initialise Shot Pointer
		Sprite* pShot = nullptr;

		//We will be using some simple physics to move our tank
		//Lets use a value for max speed for our tank to be 100
		//our cut off speed can be set to 5 so if we are below that then don't move
		float fCurrentVelocity = 0.f;
		float fMaxVelocity = 10.f;
		float fDrag = 0.03f;
		do 
		{
			float fTime = UG::GetDeltaTime();

			//Variables to hold Turret position and rotation in Vectors
			Matrix3 TurretTransform = Matrix3::IDENTITY;
			Vector3 TurretPosition;
			Vector3 TurretFwd;

			//Create a vector to store our movement
			Vector3 movementVector = Vector3::AXIS_Y;
			//We're going to be treating moving forward and backward as traversing along the sprite's Y axis
			//If our sprite was rotated 90 degrees on the sprite sheet then we would treat that direction as forward.
			float fAccelleration = 0.f;

			if (UG::IsKeyDown(UG::KEY_W))
			{
				fAccelleration += 3.f;
				fDrag = 0.f;
			}
			if (UG::IsKeyDown(UG::KEY_S))
			{
				fAccelleration -= 2.f;
				fDrag = 0.f;
			}
			if( !UG::IsKeyDown(UG::KEY_S) && !UG::IsKeyDown(UG::KEY_W))
			{
				fDrag = 0.08f;
			}

			fCurrentVelocity += fAccelleration * fTime;
			fCurrentVelocity -= fCurrentVelocity * fDrag;
			if (fabsf(fCurrentVelocity) > fMaxVelocity)
			{
				fCurrentVelocity = fMaxVelocity * Signf(fCurrentVelocity);
			}
			if (fabsf(fCurrentVelocity) > EPSILON)
			{
				movementVector *= fCurrentVelocity;
				pTank->MoveSprite(movementVector);
			}

			//Tank Rotation
			if (UG::IsKeyDown(UG::KEY_A))
			{
				pTank->RotateZ(0.05f);
			}
			if (UG::IsKeyDown(UG::KEY_D))
			{
				pTank->RotateZ(-0.05f);
			}

			//Turret Rotation
			if (UG::IsKeyDown(UG::KEY_Q))
			{
				pTurret->RotateZ(0.085f);
			}
			if (UG::IsKeyDown(UG::KEY_E))
			{
				pTurret->RotateZ(-0.085f);
			}

			// quit our application when escape is pressed
			if (UG::IsKeyDown(UG::KEY_ESCAPE))
			{
				UG::Close();
			}
			UG::ClearScreen();

			//Shooting
			if (UG::IsKeyDown(UG::KEY_SPACE))
			{
				//Draw the show when the space key is pressed and set the Turret as the parent.  Draw the shot over the tank + turret
				Vector2 SpritePos(0.5f, 0.5f);
				pShot = new Sprite("./images/ball.png", 13, 13, SpritePos, Vector4(1.f, 1.f, 0.f, 0.f));
				pShot->SetParent(pTurret);
				pShot->MarkForDraw();
				pShot->SetLayer(2);			
			}

			Vector3 ShotMove = Vector3::AXIS_Y;

			if (pShot != nullptr)	//If there is a pShot active
			{
				//Move the shot in the forward (y axis) direction
				pShot->MoveSprite(ShotMove*5.f);
				//Remove the pShot pointer as a child so that as the turret rotaties the shot still moves "forward"
				pTurret->RemoveChild(pShot);
			}

			Matrix3 tx = Matrix3::IDENTITY;

			pTurret->GetWorldTransform(tx);
			Vector3 turretRight;
			tx.GetRow(turretRight, 0);
			Vector3 turretFwd;
			tx.GetRow(turretFwd, 1);
			Vector3 turretPos;
			tx.GetRow(turretPos, 2);

			//Lines for the turret
			UG::DrawLine(turretPos.fx, turretPos.fy, turretPos.fx + turretFwd.fx * 64, turretPos.fy + turretFwd.fy * 64);
			UG::DrawLine(turretPos.fx, turretPos.fy, turretPos.fx + turretRight.fx * 64, turretPos.fy + turretRight.fy * 64, 0xFF0000FF);

			pTank->Update(fTime);
			pTurret->Update(fTime);

			if (pShot != nullptr)		//If there is a pShot active
				pShot->Update(fTime);	//Update

		} while (UG::Process());

				
		UG::Dispose();


	}
	return 0;
}
